/*
 * DUnitTests.c
 *
 *  Created on: May 3, 2017
 *      Author: srini
 */

#include "DUnitTests.h"

TESTCASE_STATS_TYPE CurrentTestCase ;
int UnitTestsInitialized = 0;

void InitializeUnitTestFramework( void )
{

}
void EnterTestCase(int _id)
{

    CurrentTestCase.Identifier = UNDEFINED_TESTCASE_ID ;
    CurrentTestCase.NumberOfTestSteps = 0 ;
    CurrentTestCase.TestStepsFailed = 0 ;
    memset(CurrentTestCase.FailedStepNumbers, 0 ,sizeof(CurrentTestCase.TestStepsFailed)) ;
    
    CurrentTestCase.Identifier = _id ;
    CurrentTestCase.NumberOfTestSteps = 0 ;
    CurrentTestCase.TestStepsFailed = 0 ;
    memset(CurrentTestCase.FailedStepNumbers,sizeof(CurrentTestCase.FailedStepNumbers),0) ;
}

void ExitTestCase()
{
    EmitExitTestCase(&CurrentTestCase) ;
    CurrentTestCase.Identifier = UNDEFINED_TESTCASE_ID ;
}

void TestStepSuccess(int teststepid)
{
     CurrentTestCase.NumberOfTestSteps++ ;
}

void TestStepFailure(int teststepid)
{
    CurrentTestCase.NumberOfTestSteps++ ;
    CurrentTestCase.TestStepsFailed++ ;
    CurrentTestCase.FailedStepNumbers[teststepid] = 1 ;
    EmitTestCaseFailure(&CurrentTestCase,teststepid) ;
    
}

void TestStepFailureInt(int teststepid, int expected, int actual )
{
    TestStepFailure(teststepid) ;
    EmitTestCaseFailedInt( &CurrentTestCase , teststepid , expected, actual ) ;
}

void TestStepFailureCharp(int teststepid, char* expected, char* actual )
{
    TestStepFailure(teststepid) ;
    EmitTestCaseFailedCharp( &CurrentTestCase , teststepid , expected, actual ) ;
}
