/*
 * EmitMessages.c
 *
 *  Created on: May 3, 2017
 *      Author: srini
 */

#include <stdio.h>

#include "DUnitTests.h"

void EmitExitTestCase(TESTCASE_STATS_TYPE *stats)
{
    printf("Test Case %d Exit\n",stats->Identifier) ;
}

void EmitTestCaseStats(TESTCASE_STATS_TYPE *stats)
{
    printf("Test Case %d Test Steps %d Faile %d=============\n",stats->Identifier,stats->NumberOfTestSteps,stats->TestStepsFailed);
}

void EmitTestCaseFailure(TESTCASE_STATS_TYPE *stats, int teststepid)
{
  printf("Test Case %d Step %d Failed\n" , stats->Identifier , teststepid ) ;
}

void EmitTestCaseFailedInt(TESTCASE_STATS_TYPE *stats, int stepid, int expected, int result )
{
    printf("Expected %d Got %d\n" , expected , result) ;
}

void EmitTestCaseFailedCharp(TESTCASE_STATS_TYPE *stats, int stepid, char* expected, char* result )
{
    printf("Expected %s Got %s\n" , expected , result) ;
}

