/*
 * main.c
 */
#include <stdio.h>
#include "DUnitTests.h"

enum
{
    TEST_FEATURE_1 ,
    TEST_FEATURE_2 ,
    NO_OF_UNIT_TESTS
};

int doubleint(int val)
{
    return val * 2 ;
}

TESTCASE (TestFeatureSet1, TEST_FEATURE_1)
    //int temp=484;
    ASSERT(1) ;
    ASSERT(0) ;
    ASSERTINT(doubleint(8),16) ;
    ASSERTSTR("hello", "hello") ;
ENDTESTCASE

TESTCASE (TestFeatureSet2, TEST_FEATURE_2)
{
    ASSERT(1) ;
    ASSERT(0) ;
    ASSERTINT(doubleint(16),16) ;
    ASSERTSTR("hello", "hello world") ;
}
ENDTESTCASE


int main(void) {
	TestFeatureSet1();
    TestFeatureSet2();
}
