/*
 * DUnitTests.h
 *
 *  Created on: May 3, 2017
 *      Author: srini
 */

#ifndef DUNITTESTS_H_
#define DUNITTESTS_H_

#include <string.h>

#define UNDEFINED_TESTCASE_ID -1
#define CORRUPTED_TESTCASE_ID -2

extern int UnitTestsInitialized;
extern void InitializeUnitTestFramework(void) ;

extern void EnterTestCase(int id) ;
extern void ExitTestCase() ;
extern void TestStepSuccess(int teststepid) ;
extern void TestStepFailure(int teststepid) ;
extern void TestStepFailureInt(int teststepid, int expected, int actual ) ;
extern void TestStepFailureCharp(int teststepid, char* expected, char* actual ) ;

#define ASSERT(_boolval) StepNo++ ; if (_boolval) TestStepSuccess(StepNo) ; else TestStepFailure(StepNo)
#define ASSERTINT(_intresult,_expected) StepNo++ ; { int tempresult=(_intresult) ; if (tempresult == _expected) TestStepSuccess(StepNo) ; else TestStepFailureInt(StepNo,_expected,tempresult) ;}
#define ASSERTSTR(_charpresult,_charpexpected) StepNo++ ; { char *tempcharp = _charpresult ; if (0 == strcmp(tempcharp,_charpexpected)) TestStepSuccess(StepNo) ; else TestStepFailureCharp(StepNo,_charpexpected,_charpresult) ;}

#define TESTCASE(_name,_id) int _name( void ) { int StepNo=0 ; int Failures=0; EnterTestCase(_id) ;
#define ENDTESTCASE ExitTestCase() ; return Failures; }

#define MAX_STEPS_PER_TESTCASE 64
typedef struct
{
  int Identifier ;
  int NumberOfTestSteps ;
  int TestStepsFailed ;
  unsigned char FailedStepNumbers[MAX_STEPS_PER_TESTCASE] ;
} TESTCASE_STATS_TYPE;

extern TESTCASE_STATS_TYPE CurrentTestCase ;

#define MAX_TESTCASES_PER_SUITE 32

typedef struct
{
    int Identifier ;
    int NumberOfTestCases ;
    int NumberOfTestCasesWithFailures ;
    unsigned char TestCasesWithFailures[MAX_TESTCASES_PER_SUITE] ;
} TESTSUITE_STATS_TYPE ;

extern TESTSUITE_STATS_TYPE CurrentTestSuite ;

extern void EmitExitTestCase(TESTCASE_STATS_TYPE *stats) ;
extern void EmitTestCaseStats(TESTCASE_STATS_TYPE *stats) ;
extern void EmitTestCaseFailure(TESTCASE_STATS_TYPE *stats, int teststepid) ;
extern void EmitTestCaseFailedInt(TESTCASE_STATS_TYPE *stats, int stepid, int expected, int result ) ;
extern void EmitTestCaseFailedCharp(TESTCASE_STATS_TYPE *stats, int stepid, char* expected, char* result ) ;

#endif /* DUNITTESTS_H_ */
